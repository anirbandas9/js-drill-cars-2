const inventory = require("./inventory.cjs");

const lastIndex = inventory.length - 1;

function problem2(inventory) {

    // if (arguments < 1 || inventory.length === 0 || !Array.isArray(inventory)) {
    //     return [];
    // }
    // return inventory.reduce((acc, currentVal, index) => {
    //     if (index === lastIndex) {
    //         return inventory[index];
    //     }
    // });
    return inventory.filter((val, index) => {
        return index === lastIndex;
    })[0];
    // return `Last car is a ${inventory[lastIndex].car_make} ${inventory[lastIndex].car_model}`;
}

// console.log(problem2(inventory));

module.exports = problem2;