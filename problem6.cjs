const inventory = require("./inventory.cjs");

function problem6(inventory) {
    // const result = [];

    if (arguments < 1 || inventory.length === 0 || !Array.isArray(inventory)) {
        return [];
    }

    // for (let index = 0; index < inventory.length; index++) {
    //     if (inventory[index].car_make === 'BMW' || inventory[index].car_make === 'Audi') {
    //         result.push(inventory[index]);
    //     }
    // }

    const result = inventory.filter((val) => {
        return val.car_make === 'BMW' || val.car_make === 'Audi';
    });

    return JSON.stringify(result);
}

// console.log(problem6(inventory));

module.exports = problem6;